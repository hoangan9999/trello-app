export const initialData = {
  boards: [
    {
      id: 'board-1',
      columnOrder: ['column-1', 'column-2', 'column-3'],
      columns: [
        {
          id: 'column-1',
          boardId: 'board-1',
          title: 'To do column',
          cardOrder: ['card-1', 'card-2', 'card-3', 'card-4', 'card-5', 'card-6', 'card-7'],
          cards: [
            {
              id: 'card-1',
              boardId: 'board-1',
              columnId: 'column-1',
              title: 'Title of card 1',
              cover: 'https://scontent-xsp1-3.xx.fbcdn.net/v/t1.6435-1/p160x160/157195704_2820552221490381_1374400710695622376_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=7206a8&_nc_ohc=L0S_VhjLXsoAX-PwpYD&_nc_ht=scontent-xsp1-3.xx&oh=df621e0d8fc8d04966b76aa1b96a17cc&oe=60F35924',
            },
            { id: 'card-2', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 2', cover: null },
            { id: 'card-3', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 3', cover: null },
            { id: 'card-4', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 4', cover: null },
            { id: 'card-5', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 5', cover: null },
            { id: 'card-6', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 6', cover: null },
            { id: 'card-7', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 7', cover: null },
          ]
        },
        {
          id: 'column-2',
          boardId: 'board-1',
          title: 'InProcess column',
          cardOrder: ['card-8', 'card-9', 'card-10'],
          cards: [
            { id: 'card-8', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 8', cover: null },
            { id: 'card-9', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 9', cover: null },
            { id: 'card-10', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 10', cover: null },
          ]
        },
        {
          id: 'column-3',
          boardId: 'board-1',
          title: 'Done column',
          cardOrder: ['card-11', 'card-12', 'card-13'],
          cards: [
            { id: 'card-11', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 11', cover: null },
            { id: 'card-12', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 12', cover: null },
            { id: 'card-13', boardId: 'board-1', columnId: 'column-1', title: 'Title of card 13', cover: null },
          ]
        }
      ]
    }
  ]
}