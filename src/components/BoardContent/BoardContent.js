import React, { useState, useEffect } from 'react'
import { Container, Draggable } from 'react-smooth-dnd';
import { isEmpty } from 'lodash';

import './BoardContent.scss'
import Column from 'components/Column/Column';

//import ultilities
import { mapOrder } from 'ultilities/sort';
import { applyDrag } from 'ultilities/dragDrop';

import { initialData } from 'actions/initialData'

function BoardContent() {
  const [board, setBoard] = useState({});
  const [columns, setColumns] = useState([]);

  useEffect(() => {
    const boardFromDB = initialData.boards.find(board => board.id === 'board-1');
    if (boardFromDB) {
      setBoard(boardFromDB)
      setColumns(mapOrder(boardFromDB.columns, boardFromDB.columnOrder, 'id'))
    }
  }, [])

  if (isEmpty(board)) {
    return <div className="not-found">Board Not Found</div>
  }

  const onColumnDrop = (dropResult) => {
    console.log(dropResult);
    let newColumns = [...columns] //destructuring clone columns ra biến mới để giá trị nó ko thay đổi
    newColumns = applyDrag(newColumns, dropResult); // sau khi xử lý xong thì sẽ set state lại columns
    setColumns(newColumns);

    let newBoard = {...board};
    newBoard.columnOrder = newColumns.map(c => c.id);
    newBoard.column = newColumns;
    setBoard(newBoard);
  }

  const onCardDrop = (columnId, dropResult)=>{
    if(dropResult.removedIndex !== null || dropResult.addedIndex !== null){
      console.log(dropResult);
      let newColumns = [...columns] 

      let currentColumn = newColumns.find(c => c.id === columnId);
      currentColumn.cards = applyDrag(currentColumn.cards, dropResult);
      currentColumn.cardOrder = currentColumn.cards.map(i => i.id);

      setColumns(newColumns)
    }
  }

  return (
    <div className="board-content">
      <Container
        orientation="horizontal" // horizontal xếp theo chiều ngang
        onDrop={onColumnDrop}
        getChildPayload={index =>
          columns[index]   //di chuyển index giữa các colums
        }
        dragHandleSelector=".column-drag-handle" //chỉ trỏ vào tiêu đề mới di chuyển dc, cần thêm class name này trong column.js
        dropPlaceholder={{
          animationDuration: 150,
          showOnTop: true,
          className: 'column-drop-preview'
        }}
      >
        {columns.map((column, index) => (
          <Draggable key={index} >
            <Column column={column}  onCardDrop={onCardDrop}/>
          </Draggable>
        ))}
      </Container>
      <div className="add-new-column">
        <i className="fa fa-plus icon" />Add another column
      </div>
    </div>
  )
}

export default BoardContent